﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using sf10_2019_pop2020.Modeli;

namespace sf10_2019_pop2020
{
    class AdresaCRUD
    {
        public DataTable CitanjeSvihAdresa()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select ulicaIbroj from adresa where aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public Boolean obrisiAdresu(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update adresa set aktivan = 0 where id=@id";
                command.Parameters.Add(new SqlParameter("id", id));

                int num = command.ExecuteNonQuery();
                if (num == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public DataTable PrikazSvihAdresa()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select * from adresa where aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }
        public Boolean IzmeniAdresu(Adresa a)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = "update adresa set ulicaIbroj=@ulica, grad=@grad, drzava=@drzava where id=@id";
                    command.Parameters.Add(new SqlParameter("ulicaIbroj", a.Ulica));
                    command.Parameters.Add(new SqlParameter("grad", a.Grad));
                    command.Parameters.Add(new SqlParameter("drzava", a.Drzava));

                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public bool DodajAdresu(Adresa a)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand commandInsert = conn.CreateCommand();
                commandInsert.CommandText = "INSERT INTO adresa (ulicaIbroj, grad, drzava, aktivan) " +
                    "values (@ulicaIbroj, @grad, @drzava, 1)";

                commandInsert.Parameters.Add(new SqlParameter("ulicaIbroj", a.Ulica));
                commandInsert.Parameters.Add(new SqlParameter("grad", a.Grad));
                commandInsert.Parameters.Add(new SqlParameter("drzava", a.Drzava));

                int num = commandInsert.ExecuteNonQuery();
                if (num >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
    }
}
