﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using sf10_2019_pop2020.Modeli;

namespace sf10_2019_pop2020
{
    class LekarCRUD
    {
        public DataTable CitanjeSvihLekara()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("DomoviZdravlja");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select k.prezime from korisnici k, domZdravlja dz, adresa a where k.domZdravlja=dz.id and dz.adresa=a.id";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public DataTable PrikazSvihLekara()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("DomoviZdravlja");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select k.ime, k.prezime, dz.naziv, a.ulicaIbroj from korisnici k, domZdravlja dz, adresa a where  k.domZdravlja=dz.id and dz.adresa=a.id ";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }



        public DataTable PretragaLekaraPoDomuZdravlja(String naziv)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("DomoviZdravlja");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select k.ime, k.prezime, dz.naziv, a.ulicaIbroj from korisnici k, domZdravlja dz, adresa a where k.domZdravlja=dz.id and dz.adresa=a.id and dz.naziv like @naziv";
                command.Parameters.Add(new SqlParameter("naziv", naziv));

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }
    }
}
