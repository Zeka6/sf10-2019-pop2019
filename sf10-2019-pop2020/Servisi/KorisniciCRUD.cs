﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using sf10_2019_pop2020.Modeli;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Data;

namespace sf10_2019_pop2020.Servisi
{
    public class KorisniciCRUD 
    {
        public String PrijavaKorisnika(String JMBG, String Lozinka)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "select tipKorisnika from korisnici where JMBG=@JMBG and lozinka=@lozinka and aktivan = 1";
                command.Parameters.Add(new SqlParameter("JMBG", Convert.ToInt64(JMBG)));
                command.Parameters.Add(new SqlParameter("lozinka", Lozinka));

                String tipKorisnika = (String)command.ExecuteScalar();
                if (tipKorisnika == null)
                {
                    return null;
                }
                else
                {
                    return tipKorisnika;
                }

            }
        }

        public bool RegistracijaKorisnika(Korisnik k, String lozinka)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "select id from adresa where ulicaIbroj=@ulicaIbroj and aktivan = 1";
                command.Parameters.Add(new SqlParameter("ulicaIbroj", k.Adresa.Ulica));
                int adresaId = (int)command.ExecuteScalar();


                SqlCommand commandInsert = conn.CreateCommand();
                commandInsert.CommandText = "INSERT INTO korisnici (jmbg, ime, prezime, lozinka, email, adresa, pol, tipKorisnika, aktivan, domZdravlja) " +
                    "values (@jmbg, @ime, @prezime, @lozinka, @email, @adresa, @pol, 'PACIJENT', 1, null)";

                commandInsert.Parameters.Add(new SqlParameter("jmbg", Convert.ToInt64(k.Jmbg)));
                commandInsert.Parameters.Add(new SqlParameter("ime", k.Ime));
                commandInsert.Parameters.Add(new SqlParameter("prezime", k.Prezime));
                commandInsert.Parameters.Add(new SqlParameter("lozinka", lozinka));
                commandInsert.Parameters.Add(new SqlParameter("email", k.Email));
                commandInsert.Parameters.Add(new SqlParameter("adresa", adresaId));
                commandInsert.Parameters.Add(new SqlParameter("pol", k.Pol.ToString()));

                int num = commandInsert.ExecuteNonQuery();
                if (num >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public bool DodajAdmina(Korisnik k, String lozinka)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "select id from adresa where ulicaIbroj=@ulicaIbroj and aktivan = 1";
                command.Parameters.Add(new SqlParameter("ulicaIbroj", k.Adresa.Ulica));
                int adresaId = (int)command.ExecuteScalar();


                SqlCommand commandInsert = conn.CreateCommand();
                commandInsert.CommandText = "INSERT INTO korisnici (jmbg, ime, prezime, lozinka, email, adresa, pol, tipKorisnika, aktivan, domZdravlja) " +
                    "values (@jmbg, @ime, @prezime, @lozinka, @email, @adresa, @pol, 'ADMINISTRATOR', 1, null)";

                commandInsert.Parameters.Add(new SqlParameter("jmbg", Convert.ToInt64(k.Jmbg)));
                commandInsert.Parameters.Add(new SqlParameter("ime", k.Ime));
                commandInsert.Parameters.Add(new SqlParameter("prezime", k.Prezime));
                commandInsert.Parameters.Add(new SqlParameter("lozinka", lozinka));
                commandInsert.Parameters.Add(new SqlParameter("email", k.Email));
                commandInsert.Parameters.Add(new SqlParameter("adresa", adresaId));
                commandInsert.Parameters.Add(new SqlParameter("pol", k.Pol.ToString()));

                int num = commandInsert.ExecuteNonQuery();
                if (num >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public bool DodajLekara(Korisnik k, String lozinka, String nazivDZ)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "select id from domZdravlja where naziv=@naziv and aktivan = 1";
                command.Parameters.Add(new SqlParameter("naziv", nazivDZ));
                int adresaId = (int)command.ExecuteScalar();


                SqlCommand commandInsert = conn.CreateCommand();
                commandInsert.CommandText = "INSERT INTO korisnici (jmbg, ime, prezime, lozinka, email, adresa, pol, tipKorisnika, aktivan, domZdravlja) " +
                    "values (@jmbg, @ime, @prezime, @lozinka, @email, @adresa, @pol, 'LEKAR', 1, @domZdravlja)";

                commandInsert.Parameters.Add(new SqlParameter("jmbg", Convert.ToInt64(k.Jmbg)));
                commandInsert.Parameters.Add(new SqlParameter("ime", k.Ime));
                commandInsert.Parameters.Add(new SqlParameter("prezime", k.Prezime));
                commandInsert.Parameters.Add(new SqlParameter("lozinka", lozinka));
                commandInsert.Parameters.Add(new SqlParameter("email", k.Email));
                commandInsert.Parameters.Add(new SqlParameter("adresa", adresaId));
                commandInsert.Parameters.Add(new SqlParameter("pol", k.Pol.ToString()));
                commandInsert.Parameters.Add(new SqlParameter("domZdravlja", adresaId));

                int num = commandInsert.ExecuteNonQuery();
                if (num >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }


        public void IzmeniInformacijeKorisnik(Korisnik k, String lozinka)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand commandAdresa = conn.CreateCommand();
                commandAdresa.CommandText = "select id from adresa where ulicaIbroj=@ulicaIbroj and aktivan = 1";
                commandAdresa.Parameters.Add(new SqlParameter("ulicaIbroj", k.Adresa.Ulica));
                int adresaId = (int)commandAdresa.ExecuteScalar();


                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update korisnici set ime=@ime, prezime=@prezime, email=@email, pol=@pol, lozinka=@lozinka, adresa=@adresa where jmbg=@jmbg";
                command.Parameters.Add(new SqlParameter("ime", k.Ime));
                command.Parameters.Add(new SqlParameter("prezime", k.Prezime));
                command.Parameters.Add(new SqlParameter("email", k.Email));
                command.Parameters.Add(new SqlParameter("pol", k.Pol.ToString()));
                command.Parameters.Add(new SqlParameter("lozinka", lozinka));
                command.Parameters.Add(new SqlParameter("jmbg", Convert.ToInt64(k.Jmbg)));
                command.Parameters.Add(new SqlParameter("adresa", adresaId));

                int num = command.ExecuteNonQuery();
            }
        }

        public Boolean obrisiKorisnika(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update korisnici set aktivan = 0 where id=@id";
                command.Parameters.Add(new SqlParameter("id", id));

                int num = command.ExecuteNonQuery();
                if (num == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public DataTable InformacijeKorisnik(String JMBG)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("InformacijaKorisnik");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select * from korisnici where JMBG=@JMBG and aktivan = 1";
                command.Parameters.Add(new SqlParameter("JMBG", Convert.ToInt64(JMBG)));

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public DataTable InformacijeSvihKorisnika()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("InformacijaKorisnik");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select * from korisnici where aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }
        public DataTable InformacijeSvihLekara()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("InformacijaKorisnik");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select jmbg from korisnici where tipKorisnika like 'LEKAR' and aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public DataTable InformacijeSvihPacijenata()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("InformacijaKorisnik");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select jmbg from korisnici where tipKorisnika like 'PACIJENT' and aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }
    }
}
