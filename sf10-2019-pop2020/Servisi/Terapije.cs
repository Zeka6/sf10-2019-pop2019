﻿using sf10_2019_pop2020.Modeli;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Servisi
{
    class Terapije
    {
        public DataTable PrikazSvihTerapija()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select * from terapija where aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public DataTable PrikazTerapijaPacijenta(String JMBG)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select k.ime, k.prezime, t.idLekara, te.opis, t.datum, t.status, t.idPacijenta  from termin t, terapija te, korisnici k where jmbg=@jmbg and t.idLekara = k.id and t.idPacijenta like '%' ";
                command.Parameters.Add(new SqlParameter("JMBG", Convert.ToInt64(JMBG)));

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public Boolean obrisiTerapiju(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update terapija set aktivan = 0 where id=@id";
                command.Parameters.Add(new SqlParameter("id", id));

                int num = command.ExecuteNonQuery();
                if (num == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool DodajTerapiju(Terapija t)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand commandL = conn.CreateCommand();
                commandL.CommandText = "select * from korisnici where jmbg=@jmbg and aktivan = 1";
                commandL.Parameters.Add(new SqlParameter("jmbg", t.Lekar.Jmbg));
                int tipKorisnikaL = (int)commandL.ExecuteScalar();

                SqlCommand commandInsert = conn.CreateCommand();
                commandInsert.CommandText = "INSERT INTO terapija (opis, idLekara,  aktivan) values (@opis, @idLekara, 1)";

                commandInsert.Parameters.Add(new SqlParameter("opis", t.OpisTerapije));
                commandInsert.Parameters.Add(new SqlParameter("idLekara", tipKorisnikaL));

                int num = commandInsert.ExecuteNonQuery();
                if (num >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
    }
}
