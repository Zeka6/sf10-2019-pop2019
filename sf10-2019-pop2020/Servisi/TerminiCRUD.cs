﻿using sf10_2019_pop2020.Modeli;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Servisi
{
    class TerminiCRUD
    {
        public DataTable PrikazSvihTermina()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select * from termin where aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public Boolean obrisiTermin(int ID)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update termin set aktivan = 0 where id=@id";
                command.Parameters.Add(new SqlParameter("ID", ID));

                int num = command.ExecuteNonQuery();
                if (num == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public Boolean zakaziTermin(int ID)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update termin set status = 'ZAKAZAN', idPacijenta=@id where id=@id and status = 'SLOBODAN'";
                command.Parameters.Add(new SqlParameter("ID", ID));

                int num = command.ExecuteNonQuery();
                if (num == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public DataTable PrikazSvihTerminaPacijenta(String JMBG)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select t.idLekara, t.datum, t.status, k.jmbg from termin t, korisnici k where jmbg=@jmbg and t.idPacijenta = k.id and t.aktivan = 1 and k.aktivan = 1";
                command.Parameters.Add(new SqlParameter("JMBG", Convert.ToInt64(JMBG)));

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public DataTable PrikazSvihSvojihTermina(String JMBG)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select t.idLekara, t.datum, t.status, k.jmbg, t.idPacijenta from termin t, korisnici k where jmbg=@jmbg and t.idLekara = k.id and t.aktivan = 1 and k.aktivan = 1 ";
                command.Parameters.Add(new SqlParameter("JMBG", Convert.ToInt64(JMBG)));

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public DataTable NadjiTerminID(Termin id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select * from termin where id=@id";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public bool DodajTermin(Termin t)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                SqlCommand commandL = conn.CreateCommand();
                commandL.CommandText = "select * from korisnici where jmbg=@jmbg and aktivan = 1";
                commandL.Parameters.Add(new SqlParameter("jmbg", t.Lekar.Jmbg));
                int tipKorisnikaL = (int)commandL.ExecuteScalar();

                SqlCommand commandInsert = conn.CreateCommand();
                commandInsert.CommandText = "INSERT INTO termin (idLekara, datum, status, idPacijenta, aktivan) values (@idLekara, @datum, 'SLOBODAN', null, 1)";

                commandInsert.Parameters.Add(new SqlParameter("idLekara", tipKorisnikaL));
                commandInsert.Parameters.Add(new SqlParameter("datum", t.Datum));

                int num = commandInsert.ExecuteNonQuery();
                if (num >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public Boolean updateTermin(Termin t)
        {
            try
            {
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
