﻿using sf10_2019_pop2020.Modeli;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sf10_2019_pop2020.Servisi;
using System.Data.SqlClient;
using System.Data;

namespace sf10_2019_pop2020.Servisi
{
    public class DomZdravljaCRUD 
    {
        public DataTable CitanjeSvihDomovaZdravlja()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("DomoviZdravlja");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select dz.naziv, a.ulicaIbroj, a.grad, a.drzava from domZdravlja dz, adresa a where dz.adresa=a.id and dz.aktivan = 1 and a.aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public DataTable PretragaDomovaZdravljaPoAdresi(String adresa)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("DomoviZdravlja");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select dz.naziv, a.ulicaIbroj, a.grad, a.drzava from domZdravlja dz, adresa a where dz.adresa=a.id and a.ulicaIbroj like @adresa and a.aktivan = 1 and dz.aktivan = 1";
                command.Parameters.Add(new SqlParameter("adresa", adresa));

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }

        public DataTable PretragaDomaZdravljaPoLekaru(String lekar)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("DomoviZdravlja");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select * from korisnici k, domZdravlja dz, adresa a where k.domZdravlja=dz.id and dz.adresa=a.id and dz.naziv like @lekar and k.aktivan = 1 and dz.aktivan = 1 and a.aktivan = 1";
                command.Parameters.Add(new SqlParameter("lekar", lekar));

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }
        public DataTable PrikazSvihDomovaZdravja()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();

                DataTable dt = new DataTable("Adrese");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();

                command.CommandText = "select * from domZdravlja where aktivan = 1";

                adapter.SelectCommand = command;
                adapter.Fill(dt);

                return dt;
            }
        }
        public bool DodajDomZdravlja(DomZdravlja dz)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "select * from adresa where ulicaIbroj=@ulicaIbroj and aktivan = 1";
                command.Parameters.Add(new SqlParameter("ulicaIbroj", dz.Adresa.Ulica));
                int adresaId = (int)command.ExecuteScalar();

                SqlCommand commandInsert = conn.CreateCommand();
                commandInsert.CommandText = "INSERT INTO domZdravlja (naziv, adresa,  aktivan) values (@naziv, @adresa, 1)";

                commandInsert.Parameters.Add(new SqlParameter("naziv", dz.Naziv));
                commandInsert.Parameters.Add(new SqlParameter("adresa", adresaId));

                int num = commandInsert.ExecuteNonQuery();
                if (num >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public Boolean obrisiDomZdravlja(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "update domZdravlja set aktivan = 0 where id=@id";
                command.Parameters.Add(new SqlParameter("id", id));

                int num = command.ExecuteNonQuery();
                if (num == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
