﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for ProzorPretraziLekara.xaml
    /// </summary>
    public partial class ProzorPretraziLekara : Window
    {
        public ProzorPretraziLekara()
        {
            InitializeComponent();
            DomZdravljaCRUD dzcrud = new DomZdravljaCRUD();
            DataTable dt1 = dzcrud.CitanjeSvihDomovaZdravlja();
            foreach (DataRow dr in dt1.Rows)
            {
                naziv.Items.Add(dr[0]);
            }

            LekarCRUD lcrud = new LekarCRUD();
            DataTable dt = lcrud.PrikazSvihLekara(); 

            DomoviZdravlja.ItemsSource = dt.DefaultView;
        }

        private void PretragaBTN_Click(object sender, RoutedEventArgs e)
        {
            DomZdravlja dz = new DomZdravlja();
            LekarCRUD lcrud = new LekarCRUD();
            naziv.SelectedItem = dz.Naziv;
            String nazivDZ = naziv.SelectedItem.ToString();
            DataTable dt = lcrud.PretragaLekaraPoDomuZdravlja(nazivDZ); 

            DomoviZdravlja.ItemsSource = dt.DefaultView;
        }

        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
