﻿using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for LekarProzor.xaml
    /// </summary>
    public partial class LekarProzor : Window
    {
        ICollectionView view;
        private DataTable dtInformacije;
        public LekarProzor(String JMBG)
        {
            InitializeComponent();
            KorisniciCRUD kcrud = new KorisniciCRUD();
            this.dtInformacije = new DataTable();

            this.dtInformacije = kcrud.InformacijeKorisnik(JMBG);

            InfoLekar.ItemsSource = dtInformacije.DefaultView;

            TerminiCRUD tcrud = new TerminiCRUD();
            view = CollectionViewSource.GetDefaultView(tcrud.PrikazSvihSvojihTermina(JMBG));
            Infotermini.ItemsSource = view;
            Infotermini.CanUserAddRows = false;

            Terapije tercrud = new Terapije();

            view = CollectionViewSource.GetDefaultView(tercrud.PrikazTerapijaPacijenta(JMBG));
            Infoterminipacijenata.ItemsSource = view;
            Infoterminipacijenata.CanUserAddRows = false;

        }
        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            Prijava pr = new Prijava();
            pr.Show();
            this.Close();
        }
        private void DomZdravljaBTN_Click(object sender, RoutedEventArgs e)
        {
            ProzorPretraziDomZdravlja pdz = new ProzorPretraziDomZdravlja();
            this.IsEnabled = false;
            pdz.ShowDialog();
            this.IsEnabled = true;
        }
        private void IzmenaBTN_Click(object sender, RoutedEventArgs e)
        {
            ProzorIzmenaSvojihPodatakaLekar pisp = new ProzorIzmenaSvojihPodatakaLekar(dtInformacije);
            pisp.Show();
            this.Close();
        }
    }
}
