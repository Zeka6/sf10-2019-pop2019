﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for IzmenaDomaZdravljaAdminProzor.xaml
    /// </summary>
    public partial class IzmenaDomaZdravljaAdminProzor : Window
    {
        private String JMBG;
        public IzmenaDomaZdravljaAdminProzor(String JMBG)
        {
            InitializeComponent();
            this.JMBG = JMBG;
            AdresaCRUD acrud = new AdresaCRUD();
            DataTable dt = acrud.CitanjeSvihAdresa();
            foreach (DataRow dr in dt.Rows)
            {
                ulica.Items.Add(dr[0]);
            }
            ulica.SelectedIndex = 0;
        }
        private void nazad(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void sacuvajIzmene(object sender, RoutedEventArgs e)
        {
            try
            {
                DomZdravlja dz = new DomZdravlja();
                dz.Naziv = naziv.Text;
                dz.Adresa.Ulica = ulica.SelectedItem.ToString();


                DomZdravljaCRUD dzrud = new DomZdravljaCRUD();
                dzrud.DodajDomZdravlja(dz);

                PrikazDomovaZdravljaAdminProzor paap = new PrikazDomovaZdravljaAdminProzor(this.JMBG);
                paap.Show();
                this.Close();
            }
            catch
            {
                naziv.Clear();

            }


        }

    }
}
