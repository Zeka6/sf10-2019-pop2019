﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for PrikazTerapijaAdminProzor.xaml
    /// </summary>
    public partial class PrikazTerapijaAdminProzor : Window
    {
        ICollectionView view;
        private String JMBG;
        public PrikazTerapijaAdminProzor(String JMBG)
        {
            this.JMBG = JMBG;
            InitializeComponent();
            Terapije tcrud = new Terapije();

            view = CollectionViewSource.GetDefaultView(tcrud.PrikazSvihTerapija());
            InfoTerapije.ItemsSource = view;
            InfoTerapije.CanUserAddRows = false;
        }

        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void DodajBTN_Click(object sender, RoutedEventArgs e)
        {
            IzmenaTerapijaAdminProzor iaap = new IzmenaTerapijaAdminProzor(JMBG);
            this.IsEnabled = false;
            iaap.ShowDialog();
            this.IsEnabled = true;
        }
        private void ObrisiBTN_Click(object sender, RoutedEventArgs e)
        {

            Terapija selektovan = view.CurrentItem as Terapija;
            Console.WriteLine(view.CurrentItem);

            if (selektovan != null)
            {
                Terapije acrud = new Terapije();
                Boolean obrisano = acrud.obrisiTerapiju(selektovan.ID);
                if (obrisano)
                {
                    PrikazTerapijaAdminProzor paap = new PrikazTerapijaAdminProzor(this.JMBG);
                    paap.Show();
                    this.Close();
                }
                else
                {
                    prikaziError("Nije bilo moguce obrisati.");
                }
            }
            else
            {
                prikaziError("Odaberite adresu koju zelite da obrisete");
            }
        }
        public void prikaziError(String error)
        {
            errorMsgLabel.Visibility = Visibility.Visible;
            errorMsgLabel.Content = error;
        }
    }
}

