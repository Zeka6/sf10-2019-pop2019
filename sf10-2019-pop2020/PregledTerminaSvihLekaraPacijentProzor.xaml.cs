﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for PregledTerminaSvihLekaraPacijentProzor.xaml
    /// </summary>
    public partial class PregledTerminaSvihLekaraPacijentProzor : Window
    {
        ICollectionView view;
        private String JMBG;
        public PregledTerminaSvihLekaraPacijentProzor(String JMBG)
        {
            this.JMBG = JMBG;
            InitializeComponent();
            TerminiCRUD tcrud = new TerminiCRUD();

            view = CollectionViewSource.GetDefaultView(tcrud.PrikazSvihTermina());

            Infotermini.ItemsSource = view;
            Infotermini.CanUserAddRows = false;
        }

        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ZakaziBTN_Click(object sender, RoutedEventArgs e)
        {
            Termin selektovan = view.CurrentItem as Termin;
            Console.WriteLine(view.CurrentItem);
            if (selektovan != null)
            {
                TerminiCRUD acrud = new TerminiCRUD();
                Boolean zakazan = acrud.zakaziTermin(selektovan.ID);
                if (zakazan)
                {
                    PacijentProzor paap = new PacijentProzor(JMBG);
                    paap.Show();
                    this.Close();
                }
                else
                {
                    prikaziError("Nije bilo moguce obrisati.");
                }
            }
            else
            {
                prikaziError("Odaberite termin koji zelite da obrisete");
            }

        }

        public void prikaziError(String error)
        {
            errorMsgLabel.Visibility = Visibility.Visible;
            errorMsgLabel.Content = error;
        }
    }
}
