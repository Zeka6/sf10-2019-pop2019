﻿using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for AdminProzor.xaml
    /// </summary>
    public partial class AdminProzor : Window
    {
        private DataTable dtInformacije;
        private String JMBG;
        public AdminProzor(String JMBG)
        {
            InitializeComponent();
            this.JMBG = JMBG;
            KorisniciCRUD kcrud = new KorisniciCRUD();
            this.dtInformacije = new DataTable();

            this.dtInformacije = kcrud.InformacijeKorisnik(JMBG);

            InfoAdmin.ItemsSource = dtInformacije.DefaultView;
        }
        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            Prijava pr = new Prijava();
            pr.Show();
            this.Close();
        }
        private void TerapijeBTN(object sender, RoutedEventArgs e)
        {
            PrikazTerapijaAdminProzor ptap = new PrikazTerapijaAdminProzor(JMBG);
            this.IsEnabled = false;
            ptap.ShowDialog();
            this.IsEnabled = true;
        }
        private void TerminiBTN(object sender, RoutedEventArgs e)
        {
            PrikazTerminaAdminProzor ptap = new PrikazTerminaAdminProzor();
            this.IsEnabled = false;
            ptap.ShowDialog();
            this.IsEnabled = true;
        }
        private void AdresaBTN(object sender, RoutedEventArgs e)
        {
            PrikazAdresaAdminProzor paap = new PrikazAdresaAdminProzor();
            this.IsEnabled = false;
            paap.ShowDialog();
            this.IsEnabled = true;
        }
        private void KorisniciBTN(object sender, RoutedEventArgs e)
        {
            PrikazKorisnikaAdminProzor pkap = new PrikazKorisnikaAdminProzor(JMBG);
            pkap.Show();
            this.Close();
        }
        private void DomZdravljaBTN(object sender, RoutedEventArgs e)
        {
            PrikazDomovaZdravljaAdminProzor pdzap = new PrikazDomovaZdravljaAdminProzor(this.JMBG);
            this.IsEnabled = false;
            pdzap.ShowDialog();
            this.IsEnabled = true;
        }
        private void IzmenaBTN_Click(object sender, RoutedEventArgs e)
        {
            ProzorIzmenaSvojihPodatakaAdmin pisp = new ProzorIzmenaSvojihPodatakaAdmin(dtInformacije);
            pisp.Show();
            this.Close();
        }
    }
}
