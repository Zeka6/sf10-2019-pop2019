﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for Prijava.xaml
    /// </summary>
    public partial class Prijava : Window
    {
        public Prijava()
        {
            InitializeComponent();
        }
        private void prijava(object sender, RoutedEventArgs e)
        {
            String JMBG = prijavaJMBG.Text;
            String Lozinka = prijavaLozinka.Text;
            long a;

            if(Int64.TryParse(JMBG, out a))
            {
                KorisniciCRUD kcrud = new KorisniciCRUD();
                String tipKorisnik = kcrud.PrijavaKorisnika(JMBG, Lozinka);
                if (tipKorisnik != null)
                {
                    if (tipKorisnik == EnumTipKorisnika.ADMINISTRATOR.ToString())
                    {
                        AdminProzor ap = new AdminProzor(JMBG);
                        ap.Show();
                    }
                    else if (tipKorisnik == EnumTipKorisnika.LEKAR.ToString())
                    {
                        LekarProzor lp = new LekarProzor(JMBG);
                        lp.Show();
                    }
                    else if (tipKorisnik == EnumTipKorisnika.PACIJENT.ToString())
                    {
                        PacijentProzor pp = new PacijentProzor(JMBG);
                        pp.Show();
                    }
                    this.Close();
                }
                else
                {
                    error.Content = "Molimo Vas da unesete tacne podatke.";
                    prijavaJMBG.Clear();
                    prijavaLozinka.Clear();
                }

            }
            else
            {
                error.Content = "Molimo Vas da unesete tacne podatke.";
                prijavaJMBG.Clear();
                prijavaLozinka.Clear();
            }
        }

        private void registracija(object sender, RoutedEventArgs e)
        {
            ProzorRegistrovani pr = new ProzorRegistrovani();
            pr.Show();
            this.Close();
        }

        private void neregistrovaniKorisnik(object sender, RoutedEventArgs e)
        {
            ProzorNeregistrovani pn = new ProzorNeregistrovani();
            pn.Show();
            this.Close();
        }

    }

}
