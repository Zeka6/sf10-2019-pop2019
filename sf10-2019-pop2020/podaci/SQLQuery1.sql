﻿create table dbo.korisnici (
jmbg bigint not null primary key, 
ime varchar(20) not null,
prezime varchar(20) not null,
korisnickoIme varchar(20) not null, 
lozinka varchar(20) not null, 
email varchar(20) not null, 
adresa varchar(30) not null, 
pol varchar(10) not null,
tipKorisnika varchar(15) not null
)