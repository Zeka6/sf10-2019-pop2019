﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for PrikazKorisnikaAdminProzor.xaml
    /// </summary>
    public partial class PrikazKorisnikaAdminProzor : Window
    {
        ICollectionView view;
        private String JMBG;
        public PrikazKorisnikaAdminProzor(String JMBG)
        {
            InitializeComponent();
            this.JMBG = JMBG;
            KorisniciCRUD kcrud = new KorisniciCRUD();

            view = CollectionViewSource.GetDefaultView(kcrud.InformacijeSvihKorisnika());
            InfoKorisnici.ItemsSource = view;
            InfoKorisnici.CanUserAddRows = false;
        }

        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            AdminProzor ap = new AdminProzor(JMBG);
            ap.Show();
            this.Close();
        }

        private void DodajBTN_Click(object sender, RoutedEventArgs e)
        {
            IzmenaKorisnikaAdminProzor ikap = new IzmenaKorisnikaAdminProzor(JMBG);
            this.IsEnabled = false;
            ikap.ShowDialog();
            this.IsEnabled = true;
        }
        private void ObrisiBTN_Click(object sender, RoutedEventArgs e)
        {
            Korisnik selektovan = view.CurrentItem as Korisnik;
            Console.WriteLine(view.CurrentItem); 

            if (selektovan != null)
            {
                //Korisnik red = (Korisnik)InfoKorisnici.SelectedItem;
                KorisniciCRUD acrud = new KorisniciCRUD();
                Boolean obrisano = acrud.obrisiKorisnika(selektovan.Id);
                if (obrisano)
                {
                    PrikazKorisnikaAdminProzor paap = new PrikazKorisnikaAdminProzor(JMBG);
                    paap.Show();
                    this.Close();
                }
                else
                {
                    prikaziError("Nije bilo moguce obrisati.");
                }
            }
            else
            {
                prikaziError("Odaberite korisnika kojeg zelite da obrisete");
            }
        }
        public void prikaziError(String error)
        {
            errorMsgLabel.Visibility = Visibility.Visible;
            errorMsgLabel.Content = error;
        }
    }
}
