﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for ProzorRegistrovani.xaml
    /// </summary>
    public partial class ProzorRegistrovani : Window
    {
        public ProzorRegistrovani()
        {
            InitializeComponent();

            AdresaCRUD acrud = new AdresaCRUD();
            DataTable dt = acrud.CitanjeSvihAdresa();
            foreach (DataRow dr in dt.Rows)
            {
                adresa.Items.Add(dr[0]);
            }
            adresa.SelectedIndex = 0;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Prijava pr = new Prijava();
            pr.Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Korisnik k = new Korisnik();
            long a;
            k.Ime = ime.Text;
            k.Prezime = prezime.Text;
            k.Jmbg = jmbg.Text;
            k.Email = email.Text;
            k.Adresa.Ulica = adresa.SelectedItem.ToString();
            if ((bool)Muski.IsChecked)
            {
                k.Pol = EnumPol.MUSKI;
            }
            else
            {
                k.Pol = EnumPol.ZENSKI;
            }
            String loz;
            loz = lozinka.Text;
            if (Int64.TryParse(k.Jmbg, out a) && k.Jmbg.Length == 13)
            {
                KorisniciCRUD kcrud = new KorisniciCRUD();
                kcrud.RegistracijaKorisnika(k, loz);
            }

            Muski.IsChecked = false;
            Zenski.IsChecked = false;
            ime.Clear();
            prezime.Clear();
            jmbg.Clear();
            email.Clear();
            lozinka.Clear();
        }
    }
}
