﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Modeli
{
    public class Admin : Korisnik
    {
        public Admin(int id, String Ime, String Prezime, String lozinka, String Email, String JMBG, Adresa Adresa, EnumPol Pol, EnumTipKorisnika TipKorisnika) : base(id, Ime, Prezime, lozinka, Email, JMBG, Adresa, Pol, TipKorisnika)
        { 

        }
    }
}
