﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Modeli
{
    public class Lekar : Korisnik
    {
        private DomZdravlja _domZdravlja;

        public DomZdravlja DomZdravlja
        {
            get { return _domZdravlja; }
            set { _domZdravlja = value; }
        }

        public Lekar(int id, String Ime, String Prezime, String lozinka, String Email, String JMBG, Adresa Adresa, EnumPol Pol, EnumTipKorisnika TipKorisnika, DomZdravlja DomZdravlja) : base(id, Ime, Prezime, lozinka, Email, JMBG, Adresa, Pol, TipKorisnika)
        {
            this.DomZdravlja = DomZdravlja;
        }

        public Lekar() : base()
        {
            DomZdravlja = new DomZdravlja(); 
        }


    }
}
