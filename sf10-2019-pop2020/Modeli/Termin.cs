﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Modeli
{
    public class Termin
    {
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private Lekar _lekar;

        public Lekar Lekar
        {
            get { return _lekar; }
            set { _lekar = value; }
        }

        private DateTime _datum;

        public DateTime Datum
        {
            get { return _datum; }
            set { _datum = value; }
        }

        private EnumStatus _status;

        public EnumStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private Pacijent _pacijent;

        public Pacijent Pacijent
        {
            get { return _pacijent; }
            set { _pacijent = value; }
        }

        public Termin()
        {
            this.Pacijent = new Pacijent();
            this.Lekar = new Lekar();
            this.Status = EnumStatus.SLOBODAN;
            this.Datum = new DateTime();
            this.ID = 0;
        }
       
    }
}
