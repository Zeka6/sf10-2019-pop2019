﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Modeli
{
    public class Terapija
    {
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _opisTerapije;

        public string OpisTerapije
        {
            get { return _opisTerapije; }
            set { _opisTerapije = value; }
        }

        private Lekar _lekar;

        public Lekar Lekar
        {
            get { return _lekar; }
            set { _lekar = value; }
        }

        public Terapija()
        {
            this.Lekar = new Lekar();
            this._id = 0;
            this.OpisTerapije = "";
        }
   

    }
}
