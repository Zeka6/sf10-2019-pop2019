﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Modeli
{
    public class Adresa
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _ulicaIbroj;

        public string Ulica
        {
            get { return _ulicaIbroj; }
            set { _ulicaIbroj = value; }
        }

        private string _drzava;

        public string Drzava
        {
            get { return _drzava; }
            set { _drzava = value; }
        }

        private string _grad;

        public string Grad
        {
            get { return _grad; }
            set { _grad = value; }
        }


        public Adresa()
        {
            Id = 0;
            Ulica = "";
            Drzava = "";
            Grad = "";
        }

    }
}
