﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;
using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;

namespace sf10_2019_pop2020.Modeli
{
    public sealed class Data
    {
        public static string ConnectionString = @"Data Source=ZEKAPEKA\SQLEXPRESS;
                                                    Integrated Security=True;
                                                    Connect Timeout=30;Encrypt=False;
                                                    TrustServerCertificate=False;ApplicationIntent=ReadWrite;
                                                    MultiSubnetFailover=False"; 

        private static readonly Data instance = new Data();


        public static Data Instance
        {
            get
            {
                return instance;
            }
        }
        public ObservableCollection<Korisnik> ListaKorisnika { get; set; }
        public ObservableCollection<Lekar> ListaLekara { get; set; }
        public ObservableCollection<DomZdravlja> ListaDomovaZdravlja { get; set; }
        public ObservableCollection<Pacijent> ListaPacijenata { get; set; }
        public ObservableCollection<Adresa> ListaAdresa { get; set; }
        public ObservableCollection<Terapija> ListaTerapija { get; set; }
        public ObservableCollection<Termin> ListaTermina { get; set; }

       
        
        
       
        
    }
}
