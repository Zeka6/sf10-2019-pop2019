﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace sf10_2019_pop2020.Modeli
{
    public class Pacijent : Korisnik
    {
        public Pacijent(int id, String Ime, String Prezime, String lozinka, String Email, String JMBG, Adresa Adresa, EnumPol Pol, EnumTipKorisnika TipKorisnika) : base(id, Ime, Prezime, lozinka, Email, JMBG, Adresa, Pol, TipKorisnika)
        {

        }

        public Pacijent() : base()
        {
            
        }

        public ObservableCollection<Termin> ListaZakazanihTermina { get; set; }

        public ObservableCollection<Terapija> ListaTerapija { get; set; }

    }

}
