﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Modeli
{
    public class DomZdravlja
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _naziv;

        public string Naziv
        {
            get { return _naziv; }
            set { _naziv = value; }
        }

        private Adresa _adresa;

        public Adresa Adresa
        {
            get { return  _adresa; }
            set { _adresa = value; }
        }

        public DomZdravlja()
        {
            Id = 0;
            Naziv = "";
            Adresa = new Adresa();
        }


    }
}

