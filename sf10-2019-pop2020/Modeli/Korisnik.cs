﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf10_2019_pop2020.Modeli
{
    public class Korisnik
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _ime;

        public string Ime
        {
            get { return _ime; }
            set { _ime = value; }
        }

        private string _prezime;

        public string Prezime
        {
            get { return _prezime; }
            set { _prezime = value; }
        }

        private string _lozinka;
                    
        public string Lozinka
        {
            get { return _lozinka; }
            set { _lozinka = value; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _jmbg;

        public string Jmbg
        {
            get { return _jmbg; }
            set { _jmbg = value; }
        }

        private Adresa _adresa;

        public Adresa Adresa
        {
            get { return  _adresa; }
            set {  _adresa = value; }
        }

        private EnumPol _pol;

        public EnumPol Pol
        {
            get { return  _pol; }
            set { _pol = value; }
        }
        private EnumTipKorisnika _tipKorisnika;

        public EnumTipKorisnika TipKorisnika
        {
            get { return _tipKorisnika; }
            set { _tipKorisnika = value; }
        }


        public Korisnik(int id, string ime, string prezime, string lozinka, string email, string jMBG, Adresa adresa, EnumPol pol, EnumTipKorisnika tipKorisnika)
        {
            Id = id;
            Ime = ime;
            Prezime = prezime;
            Lozinka = lozinka;
            Email = email;
            Jmbg = jMBG;
            Adresa = adresa;
            Pol = pol;
            TipKorisnika = tipKorisnika;
        }

        public Korisnik()
        {
            Id = 0;
            Ime = "";
            Prezime = "";
            Lozinka = "";
            Email = "";
            Jmbg = "";
            Adresa = new Adresa();
            Pol = EnumPol.MUSKI;
            TipKorisnika = EnumTipKorisnika.ADMINISTRATOR;
        }

    }
}
