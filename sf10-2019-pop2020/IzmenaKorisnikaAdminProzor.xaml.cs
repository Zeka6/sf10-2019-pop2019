﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System.Data;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for IzmenaKorisnikaAdminProzor.xaml
    /// </summary>
    public partial class IzmenaKorisnikaAdminProzor : Window
    {
        private String JMBG;
        public IzmenaKorisnikaAdminProzor(String JMBG)
        {
            InitializeComponent();
            this.JMBG = JMBG;
            AdresaCRUD acrud = new AdresaCRUD();
            DataTable dt = acrud.CitanjeSvihAdresa();
            foreach (DataRow dr in dt.Rows)
            {
                adresa.Items.Add(dr[0]);
            }
            adresa.SelectedIndex = 0;

            tipKorisnika.Items.Add(EnumTipKorisnika.PACIJENT.ToString());
            tipKorisnika.Items.Add(EnumTipKorisnika.LEKAR.ToString());
            tipKorisnika.Items.Add(EnumTipKorisnika.ADMINISTRATOR.ToString());
            tipKorisnika.SelectedIndex = 0;

            DomZdravljaCRUD dcrud = new DomZdravljaCRUD();
            DataTable dtd = dcrud.CitanjeSvihDomovaZdravlja();
            foreach (DataRow dr in dtd.Rows)
            {
                domZdravlja.Items.Add(dr[0]);
            }
            domZdravlja.IsEnabled = false;
        }
        private void nazad(object sender, RoutedEventArgs e)
        {
            AdminProzor apr = new AdminProzor(this.JMBG);
            this.Show();
            this.Close();
        }

        private void sacuvajIzmene(object sender, RoutedEventArgs e)
        {
            Korisnik k = new Korisnik();
            long a;
            k.Ime = ime.Text;
            k.Prezime = prezime.Text;
            k.Jmbg = jmbg.Text;
            k.Email = email.Text;
            k.Adresa.Ulica = adresa.SelectedItem.ToString();
            if ((bool)Muski.IsChecked)
            {
                k.Pol = EnumPol.MUSKI;
            }
            else
            {
                k.Pol = EnumPol.ZENSKI;
            }
            String loz;
            loz = lozinka.Text;
            k.TipKorisnika = (EnumTipKorisnika) Enum.Parse(typeof(EnumTipKorisnika), tipKorisnika.SelectedItem.ToString());
            String nazivDz = domZdravlja.SelectedItem.ToString();
            if (Int64.TryParse(k.Jmbg, out a) && k.Jmbg.Length == 13)
            {
                KorisniciCRUD kcrud = new KorisniciCRUD();
                if(k.TipKorisnika == EnumTipKorisnika.LEKAR)
                {
                    kcrud.DodajLekara(k, loz, nazivDz);
                    AdminProzor apr = new AdminProzor(this.JMBG);
                    this.Show();
                    this.Close();
                }
                else if(k.TipKorisnika == EnumTipKorisnika.PACIJENT)
                {
                    kcrud.RegistracijaKorisnika(k, loz);
                    AdminProzor apr = new AdminProzor(this.JMBG);
                    this.Show();
                    this.Close();
                }
                else
                {
                    kcrud.DodajAdmina(k, loz);
                    AdminProzor apr = new AdminProzor(this.JMBG);
                    this.Show();
                    this.Close();

                }
            }

            Muski.IsChecked = false;
            Zenski.IsChecked = false;
            ime.Clear();
            prezime.Clear();
            jmbg.Clear();
            email.Clear();
            lozinka.Clear();
        }

        private void tipKorisnika_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tipKorisnika.SelectedItem.ToString() == EnumTipKorisnika.PACIJENT.ToString())
            {
                domZdravlja.IsEnabled = false;
                domZdravlja.SelectedIndex = 0;
            }
            else if (tipKorisnika.SelectedItem.ToString() == EnumTipKorisnika.LEKAR.ToString())
            {
                domZdravlja.IsEnabled = true;
                domZdravlja.SelectedIndex = 0;
            }
            else
            {
                domZdravlja.IsEnabled = false;
                domZdravlja.SelectedIndex = 0;
            }
        }
    }
}
