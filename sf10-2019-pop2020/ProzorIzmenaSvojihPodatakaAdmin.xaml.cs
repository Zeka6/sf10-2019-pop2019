﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for ProzorIzmenaSvojihPodatakaAdmin.xaml
    /// </summary>
    public partial class ProzorIzmenaSvojihPodatakaAdmin : Window
    {
        public ProzorIzmenaSvojihPodatakaAdmin(DataTable dtInfomacije)
        {
            InitializeComponent();
            AdresaCRUD acrud = new AdresaCRUD();
            DataTable dt = acrud.CitanjeSvihAdresa();
            foreach (DataRow dr in dt.Rows)
            {
                adresa.Items.Add(dr[0]);
            }
            adresa.SelectedIndex = 0;

            ime.Text = dtInfomacije.Rows[0]["ime"].ToString();
            prezime.Text = dtInfomacije.Rows[0]["prezime"].ToString();
            lozinka.Text = dtInfomacije.Rows[0]["lozinka"].ToString();
            email.Text = dtInfomacije.Rows[0]["email"].ToString();
            String pol = dtInfomacije.Rows[0]["pol"].ToString();
            jmbg.Text = dtInfomacije.Rows[0]["jmbg"].ToString();
            jmbg.IsEnabled = false;
            if (pol == EnumPol.MUSKI.ToString())
            {
                Muski.IsChecked = true;
                Zenski.IsChecked = false;
            }
            else
            {
                Muski.IsChecked = false;
                Zenski.IsChecked = true;
            }
        }
        private void nazad(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void sacuvajIzmene(object sender, RoutedEventArgs e)
        {
            Korisnik k = new Korisnik();
            long a;
            k.Ime = ime.Text;
            k.Prezime = prezime.Text;
            k.Email = email.Text;
            k.Jmbg = jmbg.Text;
            k.Adresa.Ulica = adresa.SelectedItem.ToString();
            if ((bool)Muski.IsChecked)
            {
                k.Pol = EnumPol.MUSKI;
            }
            else
            {
                k.Pol = EnumPol.ZENSKI;
            }
            String loz;
            loz = lozinka.Text;
            if (Int64.TryParse(k.Jmbg, out a) && k.Jmbg.Length == 13)
            {
                KorisniciCRUD kcrud = new KorisniciCRUD();
                kcrud.IzmeniInformacijeKorisnik(k, loz);
            }

            Muski.IsChecked = false;
            Zenski.IsChecked = false;
            ime.Clear();
            prezime.Clear();
            email.Clear();
            lozinka.Clear();

            AdminProzor prp = new AdminProzor(k.Jmbg);
            prp.Show();
            this.Close();
        }
    }
}
