﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for IzmenaTerminaAdminProzor.xaml
    /// </summary>
    public partial class IzmenaTerminaAdminProzor : Window
    {
        public IzmenaTerminaAdminProzor()
        {
            InitializeComponent();

            KorisniciCRUD kcrud = new KorisniciCRUD();
            DataTable dt = kcrud.InformacijeSvihLekara();
            foreach ( DataRow dr in dt.Rows)
            {
                lekar.Items.Add(dr[0]);
            }
            lekar.SelectedIndex = 0;


        }
        private void nazad(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void sacuvajIzmene(object sender, RoutedEventArgs e)
        {
            try
            {
                Termin t = new Termin();
                DateTime pocetak = (DateTime) datumTermina.SelectedDate;

                t.Datum = pocetak;
                t.Lekar.Jmbg = lekar.SelectedItem.ToString();

                TerminiCRUD tcrud = new TerminiCRUD();
                tcrud.DodajTermin(t);

                PrikazTerminaAdminProzor ptap = new PrikazTerminaAdminProzor();
                ptap.Show();
                this.Close();

            }
            catch
            {
                

            }


        }
    }
}
