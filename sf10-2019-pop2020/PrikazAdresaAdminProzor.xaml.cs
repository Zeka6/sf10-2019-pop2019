﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using sf10_2019_pop2020.Modeli;
using System.ComponentModel;

namespace sf10_2019_pop2020
{
    
    public partial class PrikazAdresaAdminProzor : Window
    {
        ICollectionView view;
        private DataTable dtInformacije;
        public PrikazAdresaAdminProzor()
        {
            InitializeComponent();

            AdresaCRUD acrud = new AdresaCRUD();

            view = CollectionViewSource.GetDefaultView(acrud.PrikazSvihAdresa());
            InfoAdrese.ItemsSource = view;
            InfoAdrese.CanUserAddRows = false;
            InfoAdrese.IsSynchronizedWithCurrentItem = true;

        }

        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void IzmenaBTN_Click(object sender, RoutedEventArgs e)
        {
           
        }
        private void ObrisiBTN_Click(object sender, RoutedEventArgs e)
        {
            Adresa selektovan = view.CurrentItem as Adresa;
            Console.WriteLine(view.CurrentItem);

            if (selektovan != null)
            {
                AdresaCRUD acrud = new AdresaCRUD();
                Boolean obrisano = acrud.obrisiAdresu(selektovan.Id);
                if (obrisano)
                {
                    PrikazAdresaAdminProzor paap = new PrikazAdresaAdminProzor();
                    paap.Show();
                    this.Close();
                }
                else
                {
                    prikaziError("Nije bilo moguce obrisati.");
                }
            }
            else
            {
                prikaziError("Odaberite adresu koju zelite da obrisete");
            }
        }
        public void prikaziError(String error)
        {
            errorMsgLabel.Visibility = Visibility.Visible;
            errorMsgLabel.Content = error;
        }

        private void DodajAdresu(object sender, RoutedEventArgs e)
        {
            IzmenaAdreseAdminProzor iaap = new IzmenaAdreseAdminProzor();
            this.IsEnabled = false;
            iaap.ShowDialog();
            this.IsEnabled = true;

        }


    }
}
