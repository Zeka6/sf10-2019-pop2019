﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for ProzorPretraziDomZdravlja.xaml
    /// </summary>
    public partial class ProzorPretraziDomZdravlja : Window
    {
        public ProzorPretraziDomZdravlja()
        {
            InitializeComponent();
            AdresaCRUD acrud = new AdresaCRUD();
            DataTable dtt = acrud.CitanjeSvihAdresa();
            foreach (DataRow dr in dtt.Rows)
            {
                adresa.Items.Add(dr[0]);
            }

            DomZdravljaCRUD dzcrud = new DomZdravljaCRUD();
            DataTable dt = dzcrud.CitanjeSvihDomovaZdravlja();

            DomoviZdravlja.ItemsSource = dt.DefaultView;
        }

        private void PretragaBTN_Click(object sender, RoutedEventArgs e)
        {
            Korisnik k = new Korisnik();
            DomZdravljaCRUD dzcrud = new DomZdravljaCRUD();
            adresa.SelectedItem = k.Adresa.Ulica;
            String nazivAdrese = adresa.SelectedItem.ToString();
            DataTable dt = dzcrud.PretragaDomovaZdravljaPoAdresi(nazivAdrese);

            DomoviZdravlja.ItemsSource = dt.DefaultView;
        }

        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
