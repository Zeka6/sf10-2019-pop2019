﻿using sf10_2019_pop2020.Modeli;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for IzmenaAdreseAdminProzor.xaml
    /// </summary>
    public partial class IzmenaAdreseAdminProzor : Window
    {
       
        public IzmenaAdreseAdminProzor()
        {
            InitializeComponent();
         
        }

        private void nazad(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void sacuvajIzmene(object sender, RoutedEventArgs e)
        {
            try {
                Adresa a = new Adresa();
                a.Ulica = ulica.Text;
                a.Grad = grad.Text;
                a.Drzava = drzava.Text;

                AdresaCRUD acrud = new AdresaCRUD();
                acrud.DodajAdresu(a);

                PrikazAdresaAdminProzor paap = new PrikazAdresaAdminProzor();
                paap.Show();
                this.Close();
            }
            catch
            {
                ulica.Clear();
                grad.Clear();
                drzava.Clear();
            }
            

        }
    }
}
