﻿using sf10_2019_pop2020.Modeli;
using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{
    /// <summary>
    /// Interaction logic for PrikazDomovaZdravljaAdminProzor.xaml
    /// </summary>
    public partial class PrikazDomovaZdravljaAdminProzor : Window
    {
        ICollectionView view;
        private String JMBG; 
        public PrikazDomovaZdravljaAdminProzor(String JMBG)
        {
            InitializeComponent();

            this.JMBG = JMBG;
            DomZdravljaCRUD dzcrud = new DomZdravljaCRUD();

            view = CollectionViewSource.GetDefaultView(dzcrud.PrikazSvihDomovaZdravja());
            InfoDZ.ItemsSource = view;
            InfoDZ.CanUserAddRows = false;
        }

        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            AdminProzor adminProzor = new AdminProzor(this.JMBG);
            adminProzor.Show();
            this.Close();
        }
        private void dodajDZ(object sender, RoutedEventArgs e)
        {
            IzmenaDomaZdravljaAdminProzor iaap = new IzmenaDomaZdravljaAdminProzor(JMBG);
            this.IsEnabled = false;
            iaap.ShowDialog();
            this.IsEnabled = true;
        }
        private void ObrisiBTN_Click(object sender, RoutedEventArgs e)
        {

            DomZdravlja selektovan = view.CurrentItem as DomZdravlja;
            Console.WriteLine(view.CurrentItem);
            if (selektovan != null)
            {
                DomZdravljaCRUD acrud = new DomZdravljaCRUD();
                Boolean obrisano = acrud.obrisiDomZdravlja(selektovan.Id);
                if (obrisano)
                {
                    PrikazDomovaZdravljaAdminProzor paap = new PrikazDomovaZdravljaAdminProzor(JMBG);
                    paap.Show();
                    this.Close();
                }
                else
                {
                    prikaziError("Nije bilo moguce obrisati.");
                }
            }
            else
            {
                prikaziError("Odaberite dom zdravlja koji zelite da obrisete");
            }
        }
        public void prikaziError(String error)
        {
            errorMsgLabel.Visibility = Visibility.Visible;
            errorMsgLabel.Content = error;
        }
    }
}
