﻿using sf10_2019_pop2020.Servisi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf10_2019_pop2020
{

    public partial class PacijentProzor : Window
    {

        ICollectionView view;
        private DataTable dtInformacije;
        private String JMBG;
        public PacijentProzor(String JMBG)
        {
            this.JMBG = JMBG;
            InitializeComponent();
            KorisniciCRUD kcrud = new KorisniciCRUD();
            this.dtInformacije = new DataTable();

            this.dtInformacije = kcrud.InformacijeKorisnik(JMBG);

            InfoPacijent.ItemsSource = dtInformacije.DefaultView;

            TerminiCRUD tcrud = new TerminiCRUD();
            view = CollectionViewSource.GetDefaultView(tcrud.PrikazSvihTerminaPacijenta(JMBG));
            Karton.ItemsSource = view;
            Karton.CanUserAddRows = false;

      
        }

        private void NazadBTN_Click(object sender, RoutedEventArgs e)
        {
            Prijava pr = new Prijava();
            pr.Show();
            this.Close();
        }

        private void LekarBTN_Click(object sender, RoutedEventArgs e)
        {
            ProzorPretraziLekara pl = new ProzorPretraziLekara();
            this.IsEnabled = false;
            pl.ShowDialog();
            this.IsEnabled = true;
        }

        private void DomZdravljaBTN_Click(object sender, RoutedEventArgs e)
        {
            ProzorPretraziDomZdravlja pdz = new ProzorPretraziDomZdravlja();
            this.IsEnabled = false;
            pdz.ShowDialog();
            this.IsEnabled = true;
        }

        private void IzmenaBTN_Click(object sender, RoutedEventArgs e)
        {
            ProzorIzmenaSvojihPodataka pisp = new ProzorIzmenaSvojihPodataka(dtInformacije);
            pisp.Show();
            this.Close();
        }

        private void TerminiBTN_Click(object sender, RoutedEventArgs e)
        {
            PregledTerminaSvihLekaraPacijentProzor ptslpp = new PregledTerminaSvihLekaraPacijentProzor(JMBG);
            this.IsEnabled = false;
            ptslpp.Show();
            this.IsEnabled = true;
        }
    }
}
