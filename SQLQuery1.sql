﻿
create table [dbo].[adresa] (
[id] int not null identity(1,1) primary key,
[ulicaIbroj] varchar(50) not null, 
[grad] varchar(30) not null,
[drzava] varchar(30) not null,
[aktivan] bit not null
)

create table [dbo].[korisnici] (
[id] int not null identity(1,1) primary key,
[jmbg] bigint not null,
[ime] varchar(20) not null,
[prezime] varchar(20) not null, 
[lozinka] varchar(20) not null,
[email] varchar(20) not null,
[adresa] int not null,
[pol] varchar(10) not null, 
[tipKorisnika] varchar(20) not null,
[status] varchar(20) not null,
foreign key(adresa) references dbo.adresa(id),
[aktivan] bit not null
)

create table [dbo].[pacijenti] (
[id] int not null identity(1,1) primary key,
[idKorisnika] int not null,
foreign key(idKorisnika) references dbo.korisnici(id),
[aktivan] bit not null
)


create table [dbo].[domZdravlja] (
[id] int not null identity(1,1) primary key,
[naziv] varchar(30) not null, 
[adresa] int not null,
foreign key(adresa) references dbo.adresa(id),
[aktivan] bit not null
)

create table [dbo].[lekari] (
[id] int not null identity(1,1) primary key,
[idKorisnika] int not null, 
[domZdravljaId] int not null,
foreign key (idKorisnika) references dbo.korisnici(id),
foreign key(domZdravljaId) references dbo.domZdravlja(Id),
[aktivan] bit not null
)

create table [dbo].[terapija] (
[id] int not null identity(1,1) primary key,
[opis] varchar(50) not null,
[idLekara] int not null,
foreign key (idLekara) references dbo.lekari(id),
[aktivan] bit not null
)

create table [dbo].[termin] (
[id] int not null identity(1,1) primary key,
[idLekara] int not null,
[datum] date not null, 
[status] varchar(15) not null,
[idPacijenta] int not null,
foreign key (idLekara) references dbo.lekari(id),
foreign key (idPacijenta) references dbo.pacijenti(id),
[aktivan] bit not null
)
